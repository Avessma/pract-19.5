﻿#include <iostream>


class Animal {
public:
	virtual void Voice() {
		std::cout << "voiceAnimal" << std::endl;
	}
};

class Dog : public Animal {
public:
	void Voice() override {
		std::cout << "Woooooof!" << std::endl;
	}
};

class Cat : public Animal {
public:
	void Voice() override {
		std::cout << "Meeeeeeoow!" << std::endl;
	}
};

class Cow : public Animal {
public:
	void Voice() override {
		std::cout << "Muuuuuuu!" << std::endl;
	}
};

class Chicken : public Animal {
public:
	void Voice() override {
		std::cout << "Kukarekuuuuu!" << std::endl;
	}
};


int main()
{
	Animal animal;
	Dog dog;
	Cat cat;
	Cow cow;
	Chicken chicken;

	const int SIZE = 4;

	Animal* Voice[SIZE] = {&dog, &cat, &cow, &chicken};

	for (int i = 0; i < SIZE; i++)
	{
		Voice[i]->Voice();
	}



	return 0;
}

